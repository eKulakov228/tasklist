let task_1 = {
    isComplete: false,
    id: 1,
    name: 'Купить хлеб'
};
//поля/свойства (ключ: значение)

const tasks = [
    {
        isComplete: false,
        id: 1,
        name: 'Купить хлеб'
    },
    {
        isComplete: false,
        id: 2,
        name: 'Сделать уборку'
    },
    {
        isComplete: false,
        id: 3,
        name: 'Похавать'
    },
    {
        isComplete: false,
        id: 4,
        name: 'Поспать'
    },
    {
        isComplete: true,
        id: 5,
        name: 'Поужинать'
    },
    {
        isComplete: false,
        id: 6,
        name: 'Еще раз поспать'
    }   
]

/*function drawTask(task) {
    return `
    <div class="task">
            <input type="checkbox" class="task__complete" ${task.isComplete ? 'checked' : ''}>
            <span class="task__number">${task.id}</span>
            <span class="task__name">${task.name}</span>
        </div>`;
}*/

const list = document.querySelector('.container__list');

/*tasks.forEach(item => {
    list.innerHTML += drawTask(item);
});

drawTask();

console.log(tasks[1].name);*/


///////////////////////////////////

function createTaskTag(task) {
    const name = document.createElement('span'); // name = <span> </spam>
    name.classList = 'task__name';
    name.innerText = task.name;

    const number = document.createElement('span');
    number.className = 'task__number';
    number.innerText = task.id;

    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.className = 'task_complete';
    checkbox.checked = task.isComplete;

    const taskTag = document.createElement('div');
    taskTag.className = 'task';
    taskTag.appendChild(checkbox);
    taskTag.appendChild(number);
    taskTag.appendChild(name);

    return taskTag;
}

tasks.forEach(item => {
    //const  myTask = createTaskTag(item);
    list.appendChild(createTaskTag(item));
});